﻿<%@ Page Language="C#" Async="true" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="DDLCascadedTask.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        td{
            padding:10px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <asp:ScriptManager ID="scriptManager" runat="server">
            </asp:ScriptManager>

            <asp:UpdatePanel ID="updatePanel" runat="server">
                <ContentTemplate>
                   <%-- <asp:Button ID="btnSubmit" runat="server"  Text="Submit"/>--%>
                        <table>
                            <tr>
                                <td>
                                    <asp:DropDownList ID="ddlCountry" runat="server" DataValueField="CountryID" DataTextField="CountryName" AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"></asp:DropDownList>
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    <asp:DropDownList ID="ddlState" runat="server" DataValueField="StateID" DataTextField="StateName" AutoPostBack="true" OnSelectedIndexChanged="ddlState_SelectedIndexChanged1"></asp:DropDownList>
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    <asp:DropDownList ID="ddlCity" runat="server" DataValueField="CityId" DataTextField="CityName"></asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                </ContentTemplate>
            </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
