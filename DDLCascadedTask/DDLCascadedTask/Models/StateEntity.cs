﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DDLCascadedTask.Models
{
    public class StateEntity
    {
        public decimal? StateID { get; set; }

        public String StateName { get; set; }

        public decimal? CountryID { get; set; }
    }
}