﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DDLCascadedTask.Models
{
    public class CountryEntity
    {
        public decimal? CountryID { get; set; }

        public String CountryName { get; set; }
    }
}