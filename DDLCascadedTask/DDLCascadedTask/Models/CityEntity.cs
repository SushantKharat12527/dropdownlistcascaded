﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DDLCascadedTask.Models
{
    public class CityEntity
    {
        public decimal? CityId { get; set; }

        public String CityName { get; set; }

        public decimal? StateID { get; set; }
    }
}