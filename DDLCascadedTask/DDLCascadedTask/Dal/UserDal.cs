﻿using DDLCascadedTask.Dal.ORD;
using DDLCascadedTask.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace DDLCascadedTask.Dal
{
    public class UserDal
    {
        #region Declaration
        private UserDCDataContext _dc = null;
        #endregion

        #region Constructor
        public UserDal()
        {
            _dc = new UserDCDataContext();
        }
        #endregion

        #region Private Property
        private Func<Country, CountryEntity> SelectTerritoryData
        {
            get
            {
                return
                     (leSalesTerritoryObj) => new CountryEntity()
                     {
                         CountryID = leSalesTerritoryObj.Country_ID,
                         CountryName = leSalesTerritoryObj.Country_Name
                     };
            }
        }

        private Func<State, StateEntity> SelectStateData
        {
            get
            {
                return
                       (leStateObj) => new StateEntity()
                       {
                           StateID = leStateObj.State_ID,
                           StateName = leStateObj.State_Name
                       };
            }
        }

        private Func<City, CityEntity> SelectCityData
        {
            get
            {
                return
                    (leCityObj) => new CityEntity()
                    {
                        CityId = leCityObj.City_ID,
                        CityName=leCityObj.City_Name
                    };
            }
        }
        #endregion 

        #region Public Methods
        public async Task<IEnumerable<CountryEntity>> GetCountryData()
        {
            return await Task.Run(() => {

                return
                    _dc
                    ?.Countries
                    ?.AsEnumerable()
                    ?.Select(this.SelectTerritoryData)
                    ?.ToList();
            });
        }

        public async Task<IEnumerable<StateEntity>> GetStateData(StateEntity stateEntityObj)
        {
            return await Task.Run(() => {

                return
                    _dc
                    ?.States
                    ?.AsEnumerable()
                    ?.Where((leStateObj) => leStateObj.Country_ID == stateEntityObj.CountryID)
                    ?.Select(this.SelectStateData)
                    ?.ToList();

            });
        }

        public async Task<IEnumerable<CityEntity>> GetCityData(CityEntity cityEntityObj)
        {
            return await Task.Run(() => {

                return
                    _dc
                    ?.Cities
                    ?.AsEnumerable()
                    ?.Where((leCityObj) => leCityObj.State_ID == cityEntityObj.StateID)
                    ?.Select(this.SelectCityData)
                    ?.ToList();
            });
        }
        #endregion 
    }
}