﻿using DDLCascadedTask.Dal;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DDLCascadedTask
{
    public partial class Default : System.Web.UI.Page
    {
        #region Declaration
        private UserDal _userDal = null;
        #endregion

        #region Constructor
        public Default()
        {
            // _userDal = new UserDal();
        }
        #endregion 
        protected async void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                // Create an Instance of UserDal Object 
                _userDal = new UserDal();

                // Store User Dal Object in View State
                this.UserDalInstance = _userDal;

                // Call Bind Territory Function
                await this.BindCountryData();
            }
        }
             #region Private Property
        private UserDal UserDalInstance
        {
            get
            {
                return
                       _userDal
                            ??
                                (JsonConvert.DeserializeObject<UserDal>(ViewState["UserDalObject"] as string));
            }
            set
            {
                _userDal = value;
                ViewState["UserDalObject"] = JsonConvert.SerializeObject(_userDal);
            }
        }
        #endregion

        #region Private Methods
        private async Task BindCountryData()
        {
            // Get Territory Data from User Dal
            var getCountryData = await this
                                                       .UserDalInstance
                                                       ?.GetCountryData();

            // Clear all Items of Dropdownlist Before Binding..
            ddlCountry.DataSource = null;
            ddlCountry.Items.Clear();

            // Bind Territory data to Dropdownlist
            ddlCountry.DataSource = getCountryData;
            ddlCountry.DataBind();

            // Set Title on DropDownList
            ddlCountry.AppendDataBoundItems = true;
            ddlCountry.Items.Insert(0, new ListItem("--Select Territory--", "0"));
            ddlCountry.SelectedIndex = 0;
        }

        private async Task BindStateData(decimal? id)
        {
            // get State Data from User Dal
            var getStateData =
                    await UserDalInstance
                             ?.GetStateData(new Models.StateEntity()
                             {
                                 CountryID = id
                             });

            // Clear all Items of Dropdownlist Before Binding..
            ddlState.DataSource = null;
            ddlState.Items.Clear();

            // Bind State data to Dropdownlist
            ddlState.DataSource = getStateData;
            ddlState.DataBind();

            // Set Title on DropDownList
            ddlState.AppendDataBoundItems = true;
            ddlState.Items.Insert(0, new ListItem("--Select State--", "0"));
            ddlState.SelectedIndex = 0;

        }

        private async Task BindCityData(decimal? id)
        {
            // get City Data from User Dal
            var getCityData =
                    await
                        this.UserDalInstance
                        ?.GetCityData(new Models.CityEntity()
                        {
                            StateID = id
                        });

            // Clear all Items of Dropdownlist Before Binding..
            ddlCity.DataSource = null;
            ddlCity.Items.Clear();

            // Bind City data to Dropdownlist
            ddlCity.DataSource = getCityData;
            ddlCity.DataBind();

            // Set Title on DropDownList
            ddlCity.AppendDataBoundItems = true;
            ddlCity.Items.Insert(0, new ListItem("--Select City--", "0"));
            ddlCity.SelectedIndex = 0;
        }
        #endregion

        #region Event

        protected async void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {

            ddlCity.DataSource = null;
            ddlCity.Items.Clear();

            await this.BindStateData(Convert.ToDecimal(ddlCountry.SelectedValue));
        }

        protected async void ddlState_SelectedIndexChanged1(object sender, EventArgs e)
        {
            await this.BindCityData(Convert.ToDecimal(ddlState.SelectedValue));
        }
        #endregion
    
    }

}
