CREATE TABLE Country (
    Country_ID int NOT NULL,
    Country_Name varchar(MAX) NOT NULL,
    PRIMARY KEY (Country_ID)
    
);

CREATE TABLE State (
    State_ID int NOT NULL,
    State_Name varchar(MAX) NOT NULL,
	Country_ID int NOT NULL,
    PRIMARY KEY (State_ID),
   FOREIGN KEY (Country_ID) REFERENCES Country(Country_ID)
);

CREATE TABLE City (
    City_ID int NOT NULL,
    City_Name varchar(MAX) NOT NULL,
	State_ID int NOT NULL,
    PRIMARY KEY (State_ID),
	FOREIGN KEY (State_ID) REFERENCES State(State_ID)
   
);

